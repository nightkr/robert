use robert::Builder;

#[derive(Builder, Debug)]
struct Foo {
    #[robert(default = "2")]
    field_one: u8,
    #[robert(default)]
    field_two: String,
    asdfasdf: (),
}

fn main() {
    Foo::builder();
    dbg!(Foo::builder().asdfasdf(()).build());
    dbg!(Foo::builder()
        .field_one(5)
        .field_two("asdf".to_string())
        .asdfasdf(())
        .build());
}
