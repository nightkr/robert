use robert::Builder;

#[derive(Builder, Debug)]
struct Foo {
    field_one: u8,
    field_two: String,
}

fn main() {
    dbg!(Foo::builder()
        .field_one(5)
        .field_two("asdf".to_string())
        .build());
}
