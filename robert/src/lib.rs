//! # Robert the Builder
//!
//! (Bob was already taken)
//!
//! Robert is a Rust derive macro that generates type-safe builders. Basically, based on this:
//!
//! ```rust
//! struct Foo {
//!     bar: u8,
//!     baz: u16,
//! }
//! ```
//!
//! It generates an associated builder type, and helper methods:
//!
//! ```rust
//! # use robert::Required;
//! #
//! # struct Foo {
//! #     bar: u8,
//! #     baz: u16,
//! # }
//! #
//! impl Foo {
//!     fn builder() -> FooBuilder<Required, Required> {
//!         FooBuilder {
//!             bar: Required,
//!             baz: Required,
//!         }
//!     }
//! }
//!
//! struct FooBuilder<Bar, Baz> {
//!     bar: Bar,
//!     baz: Baz,
//! }
//!
//! impl<Bar, Baz> FooBuilder<Bar, Baz> {
//!     fn bar(self, bar: u8) -> FooBuilder<u8, Baz> {
//!         FooBuilder { bar, baz: self.baz }
//!     }
//!
//!     fn baz(self, baz: u16) -> FooBuilder<Bar, u16> {
//!         FooBuilder { bar: self.bar, baz }
//!     }
//! }
//!
//! impl FooBuilder<u8, u16> {
//!     fn build(self) -> Foo {
//!         Foo { bar: self.bar, baz: self.baz }
//!     }
//! }
//! ```
//!
//! Thus, the following two ways to build `Foo` are equivalent:
//!
//! ```rust
//! # use robert::Builder;
//! #[derive(Builder, Debug, Eq, PartialEq)]
//! struct Foo {
//!     bar: u8,
//!     baz: u16,
//! }
//!
//! assert_eq!(
//!     Foo::builder()
//!         .bar(4)
//!         .baz(8)
//!         .build(),
//!     Foo { bar: 4, baz: 8 },
//! );
//! ```
//!
//! # Motivation
//!
//! ## Optionals
//!
//! Rust doesn't really allow defining optional fields. Consider the following struct:
//!
//! ```rust
//! # struct TrustDb;
//! struct TlsConfig {
//!     trust_db: TrustDb,
//!     unsafe_allow_expired_certificates: bool,
//! }
//! ```
//!
//! Here, we'd probably like to require `trust_db` (since it's pretty difficult to connect anywhere if we can't validate certificates),
//! while we'd like to bury `unsafe_allow_expired_certificates` unless it's actually required by the user.
//!
//! Sadly, Rust only allows us to define the whole struct as `Default`, meaning that for a bare struct we either have to specify all fields exhaustively:
//!
//! ```rust
//! # struct TrustDb;
//! # fn get_trust_db() -> TrustDb { TrustDb }
//! struct TlsConfig {
//!     trust_db: TrustDb,
//!     unsafe_allow_expired_certificates: bool,
//! }
//! TlsConfig {
//!     trust_db: get_trust_db(),
//!     // Meh, annoying
//!     unsafe_allow_expired_certificates: false,
//! };
//! ```
//!
//! Or we lose the ability to demand fields at all:
//!
//! ```rust
//! # #[derive(Default)]
//! # struct TrustDb;
//! #[derive(Default)]
//! struct TlsConfig {
//!     trust_db: TrustDb,
//!     unsafe_allow_expired_certificates: bool,
//! }
//! TlsConfig {
//!     // OOPS: forgot to add a trust DB
//!     // trust_db: get_trust_db(),
//!     ..TlsConfig::default()
//! };
//! ```
//!
//! We could define a builder manually with a validation step, but that's annoying busywork, and often ends up deferring what should be a compile error
//! into a runtime error:
//!
//! ```rust
//! # #[derive(Debug, Eq, PartialEq)]
//! # struct TrustDb;
//! # fn get_trust_db() -> TrustDb { TrustDb }
//! # #[derive(Debug, Eq, PartialEq)]
//! # struct TlsConfig {
//! #     trust_db: TrustDb,
//! #     unsafe_allow_expired_certificates: bool,
//! # }
//! impl TlsConfig {
//!     fn builder() -> TlsConfigBuilder {
//!         TlsConfigBuilder {
//!             trust_db: None,
//!             // Default values!
//!             unsafe_allow_expired_certificates: false,
//!         }
//!     }
//! }
//! struct TlsConfigBuilder {
//!     trust_db: Option<TrustDb>,
//!     unsafe_allow_expired_certificates: bool,
//! }
//! impl TlsConfigBuilder {
//!     fn trust_db(self, trust_db: TrustDb) -> Self {
//!         Self { trust_db: Some(trust_db), ..self }
//!     }
//!
//!     fn unsafe_allow_expired_certificates(self,
//!         unsafe_allow_expired_certificates: bool,
//!     ) -> Self {
//!         Self { unsafe_allow_expired_certificates, ..self }
//!     }
//!
//!     fn build(self) -> Option<TlsConfig> {
//!         Some(TlsConfig {
//!             // In reality, you'd hopefully have some more descriptive error handling here..
//!             trust_db: self.trust_db?,
//!             unsafe_allow_expired_certificates: self.unsafe_allow_expired_certificates,
//!         })
//!     }
//! }
//!
//! assert_eq!(
//!     TlsConfig::builder()
//!         .trust_db(get_trust_db())
//!         .build(),
//!     Some(TlsConfig {
//!         trust_db: get_trust_db(),
//!         unsafe_allow_expired_certificates: false
//!     })
//! );
//! assert_eq!(
//!     // OOPS: Forgot to add trust_db
//!     TlsConfig::builder().build(),
//!     None,
//! );
//! ```
//!
//! Robert uses the type system to enforce that all required fields have been set before the object can be built.
//!
//! ## Generics
//!
//! Another case that manual builders handle quite poorly is generics. For example, take this struct (effectively a vtable):
//!
//! ```rust
//! # struct Foo;
//! # struct Bar;
//! # struct Error;
//! struct Controller<Reconciler, ErrorPolicy>
//! where
//!     Reconciler: Fn(Foo) -> Result<Bar, Error>,
//!     ErrorPolicy: Fn(Error),
//! {
//!     reconciler: Reconciler,
//!     error_policy: ErrorPolicy,
//! }
//! ```
//!
//! A builder would either have to juggle around these generics for every setter (exponentially annoying and error-prone, with the number
//! of fields), or it would have to box the objects into trait objects (forcing extra allocations, and preventing the compiler from inlining them):
//!
//! ```rust
//! # struct Foo;
//! # struct Bar;
//! # struct Error;
//! struct Controller {
//!     reconciler: Box<dyn Fn(Foo) -> Result<Bar, Error>>,
//!     error_policy: Box<dyn Fn(Error)>,
//! }
//! ```
//!
//! Robert is designed to handle this generic juggling for you, giving you the flexibility and efficiency of generics while (mostly) preserving
//! the ergonomics of boxing.

pub use robert_macros::Builder;

/// Marker type for required fields that have not yet been given a value.
pub struct Required;
