use proc_macro2::TokenStream;
use quote::{format_ident, quote, ToTokens};
use syn::{parse_quote, Expr, Field, Ident, ItemStruct, Lit, Meta, NestedMeta, Type};

struct GenericGen {
    counter: u8,
}

impl GenericGen {
    fn new() -> Self {
        Self { counter: 0 }
    }

    fn next(&mut self) -> Ident {
        let ident = format_ident!("Arg{}", self.counter);
        self.counter += 1;
        ident
    }
}

struct BuilderField<'a> {
    ident: &'a Ident,
    tpe: Ident,
    target_type: &'a Type,
    default_type: Type,
    default_value: Expr,
}
type BF<'a> = BuilderField<'a>;

/// Generates a builder struct, named `${MyStruct}Builder`, containing a field, generic, and setter for each field
/// on the deriving struct. Also generates a `MyStruct::builder()` method to create a new builder instance,
/// and a `MyStructBuilder::build()` to build a finished instance once the builder is in a "valid" state (no `Required` fields remain).
#[proc_macro_derive(Builder, attributes(robert))]
pub fn builder(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let ItemStruct {
        ident: struct_ident,
        fields,
        ..
    } = syn::parse_macro_input!(input);
    let required: Type = parse_quote!(::robert::Required);
    let required_expr: Expr = parse_quote!(::robert::Required);
    let mut generic_gen = GenericGen::new();
    let builder_ident = format_ident!("{}Builder", struct_ident, span = struct_ident.span());
    let struct_fields = fields
        .iter()
        .map(
            |Field {
                 ident,
                 ty: target_type,
                 attrs,
                 ..
             }| {
                let tpe = generic_gen.next();
                let mut default_type = required.clone();
                let mut default_value = required_expr.clone();
                for attr in attrs {
                    let attr = attr.parse_meta().unwrap();
                    match attr {
                        _ if !attr.path().is_ident("robert") => {
                            // Not ours, ignore
                        }
                        Meta::Path(_) => {
                            // No-op, ignore
                        }
                        Meta::List(attr) => {
                            for sub_attr in attr.nested {
                                match sub_attr {
                                    NestedMeta::Meta(default_attr)
                                        if default_attr.path().is_ident("default") =>
                                    {
                                        default_type = target_type.clone();
                                        default_value = match default_attr {
                                            Meta::NameValue(default_attr) => {
                                                match default_attr.lit {
                                                    Lit::Str(default_attr_lit) => {
                                                        default_attr_lit.parse().unwrap()
                                                    }
                                                    _ => panic!(
                                                        "default value must be a string (expression)"
                                                    ),
                                                }
                                            }
                                            Meta::Path(_) => {
                                                parse_quote!(::core::default::Default::default())
                                            }
                                            Meta::List(_) => panic!("illegal form of #[robert(default)]"),
                                        }
                                    }
                                    _ => panic!(
                                        "invalid #[robert] subattribute: {}",
                                        sub_attr.into_token_stream()
                                    ),
                                }
                            }
                        }
                        Meta::NameValue(_) => panic!(
                            "#[robert] attribute must only be used in list form (#[robert(...)])"
                        ),
                    }
                }
                BuilderField {
                    ident: ident.as_ref().unwrap(),
                    tpe,
                    target_type,
                    default_type,
                    default_value,
                }
            },
        )
        .collect::<Vec<_>>();
    let struct_field_defs = struct_fields
        .iter()
        .map(|BF { ident, tpe, .. }| quote!(#ident: #tpe,))
        .collect::<TokenStream>();
    let struct_field_names = struct_fields
        .iter()
        .map(|BF { ident, .. }| quote!(#ident,))
        .collect::<TokenStream>();
    let generic_defs = struct_fields
        .iter()
        .map(|BF { tpe, .. }| quote!(#tpe,))
        .collect::<TokenStream>();
    let generics_defaults = struct_fields
        .iter()
        .map(|BF { default_type, .. }| quote!(#default_type,))
        .collect::<TokenStream>();
    let generics_defined = struct_fields
        .iter()
        .map(|BF { target_type, .. }| quote!(#target_type,))
        .collect::<TokenStream>();
    let struct_field_defaults = struct_fields
        .iter()
        .map(
            |BF {
                 ident,
                 default_value,
                 ..
             }| quote!(#ident: #default_value,),
        )
        .collect::<TokenStream>();
    let setters = struct_fields
        .iter()
        .map(
            |BF {
                 ident, target_type, ..
             }| {
                let updated_generics = struct_fields
                    .iter()
                    .map(
                        |BF {
                             ident: generic_ident,
                             tpe,
                             target_type,
                             ..
                         }| {
                            if generic_ident == ident {
                                quote!(#target_type,)
                            } else {
                                quote!(#tpe,)
                            }
                        },
                    )
                    .collect::<TokenStream>();
                let other_fields = struct_fields
                    .iter()
                    .filter(|other_field| other_field.ident != *ident)
                    .map(|BF { ident, .. }| quote!(#ident,))
                    .collect::<TokenStream>();
                quote! {
                    fn #ident(self, #ident: #target_type) -> #builder_ident<#updated_generics> {
                        let #builder_ident { #ident: _, #other_fields } = self;
                        #builder_ident { #ident, #other_fields }
                    }
                }
            },
        )
        .collect::<TokenStream>();
    let all = quote! {
        impl #struct_ident {
            fn builder() -> #builder_ident<#generics_defaults> {
                #builder_ident {
                    #struct_field_defaults
                }
            }
        }

        pub struct #builder_ident<#generic_defs> {
            #struct_field_defs
        }

        impl<#generic_defs> #builder_ident<#generic_defs> {
            #setters
        }

        impl #builder_ident<#generics_defined> {
            fn build(self) -> #struct_ident {
                let #builder_ident { #struct_field_names } = self;
                #struct_ident { #struct_field_names }
            }
        }
    };
    // println!("{}", all);
    all.into()
}
